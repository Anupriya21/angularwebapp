import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../app.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  res2$: Observable<any>;
  text$: Observable<any>;
  res3$: Observable<any>;
  res4$: Observable<any>;
  res5$: Observable<any>;
  res2;
  res3;
  res4;
  res5;
  Synonyms;
  LogForm;
  sentences;
  modified = [];
  ActualAPIStatus = {};
  wordsen = [];
  Response2 = {};
  Response5 = {};
  Response4 = [
    {
      "Phones": [],
      "Emails": [
        "firstaidtraining@redcross.org.uk"
      ],
      "PPSs": []
    }
  ];
  Response3 = {
    "derived_date": "2019-12-13 10:41:21.637561",
    "positive": 0.125,
    "neutral": 0.852,
    "negative": 0.023,
    "compound": 0.9963,
    "verdict": "NEUTRAL",
    "verdict_compound": "POSITIVE"
  };
  Response1 = {
    "named_entity_resolution": {
      "People": [
        "Filippo",
        "Filippo Sassi",
        "Patrick Gollop",
        "Bradbury House",
        "Filippo Sassi",
        "Filippo",
        "Filippo Sassi Pass",
        "Da GAQ",
        "Patrick Gollop",
        "Scotiand"
      ],
      "Companies, agencies, institutions": [
        "the British Red Cross",
        "redcross.org.uk/safehands",
        "Dontforget",
        "the Customer Service Team",
        "Mo Aton",
        "the Heai) & Safety",
        "the Intemational Red Cross",
        "Red Crescent Movement",
        "The Bish Red Gross Society",
        "Red Cross Training Regitiofad",
        "Ohio Avenue Salford",
        "M50",
        "Blackthorn Way Ashford",
        "redcross.org.uk/safehands",
        "the British Red Cross",
        "the Customer Service Team",
        "Red Cross Training",
        "the International Red Cross",
        "Red Crescent Movement",
        "The British Red Cross Society",
        "Registered"
      ],
      "Nationalities or religious or political groups": [],
      "Buildings, airports, highways, bridges": [],
      "Countries, cities, states": [
        "England",
        "Wales",
        "Scotland",
        "England",
        "Wales"
      ],
      "Monetary values, including unit": [],
      "Non-GPE locations, mountain ranges, bodies of water": [],
      "Objects, vehicles, foods": [],
      "Named hurricanes, battles, wars, sports events": [
        "Aid Training 00045 4"
      ],
      "Titles of books, songs, arts": [
        "Safe Hands",
        "Version",
        "Safe Hands"
      ],
      "Laws": [],
      "Languages": [],
      "Dates": [
        "30 Aug 2016",
        "Date 29 Aug 2019",
        "1981",
        "arter 1908"
      ],
      "Times": [],
      "Percentages": [
        "0844 412 2739"
      ],
      "Quantities": [],
      "Ordinals": [
        "first",
        "first",
        "first",
        "First",
        "first"
      ],
      "Cardinals": [
        "two",
        "One",
        "up to 12",
        "0844 871",
        "220949",
        "19",
        "one",
        "1908",
        "220949"
      ]
    }
  };
  constructor(private appService: AppService) { }

  ngOnInit() {
    this.LogForm = new FormGroup({
      Keyword: new FormControl('')
    });
    for (let key in this.Response1["named_entity_resolution"]) {
      if (this.Response1["named_entity_resolution"][key].length) {
        this.Response2[key] = this.Response1["named_entity_resolution"][key];
      }
    }
    for (let key in this.Response4[0]) {
      if (this.Response4[0][key].length) {
        this.Response5[key] = this.Response4[0][key];
      }
    }
    this.appService.isSentences.subscribe(res => {
      this.sentences = res;
    });
    this.appService.isActualAPIStatus.subscribe(res => {
      this.ActualAPIStatus = res;
    });
    this.appService.isRes2.subscribe(res => {
      this.res2 = res;
    });
    this.res3 = { "named_entity_resolution": { "found": ["Joe Black"] } };
    this.res4 = [{ "phones": [], "Mails": ["abc@gmail.con", "abc@libero.it"], "pPSs": ["8765432A", "1234567F"] }];
    this.res5 = { "KEYWORD": [true, ["hated", "come", "Example", "night", "Boring", "example", "disgusting", "awful", "horrible", "Simple", "food"]] };
  }
  makeBold(input, wordsToBold) {
    return input.replace(new RegExp('(\\b)(' + wordsToBold.join('|') + ')(\\b)', 'ig'), '$1<b>$2</b>$3');
  }
  submit() {
    console.log(this.LogForm.get('Keyword').value);
    this.appService.getSynonyms(this.LogForm.get('Keyword').value).subscribe(res => {
      console.log(res);
      this.Synonyms = res["adjective"]["sim"];
      console.log(this.Synonyms);
      let tempdic = {};
      tempdic[this.sentences] = [];
      let wordToBold = [];
      let words = this.sentences.split(" ");
      for (let w of words) {
        for (let s of this.Synonyms) {
          if (w.replace(/[^a-zA-Z ]/g, "").match(RegExp(s))) {
            wordToBold.push(w.replace(/[^a-zA-Z ]/g, ""));
            break;
          }
        }
      }
      wordToBold.push(this.LogForm.get('Keyword').value);
      tempdic[this.sentences] = wordToBold;
      this.wordsen.push(tempdic);
      document.getElementById('sentenceText').innerHTML = this.makeBold(Object.keys(this.wordsen[0])[0], this.wordsen[0][Object.keys(this.wordsen[0])[0]]);
    });
  }
}
