import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { ResultComponent } from "./result/result.component";
import { APISelectionComponent } from './apiselection/apiselection.component';
const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "result", component: ResultComponent },
  { path: "Select", component:APISelectionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
