import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class AppService {
  url1             = "http://localhost:51598/api/API1/";
  url2             = "https://api.dandelion.eu/datatxt/sent/v1?token=64b5a0db6a134b578221baee3a6f2679&text=";
  url3             = "http://localhost:51598/api/API3/";//Put RegexURL Here................
  url4             = "http://localhost:51598/api/API4/";
  url5             = "http://localhost:51598/api/API5/";
  url6             = "http://localhost:51598/api/API6/";
  url7             = "https://words.bighugelabs.com/api/2/322df4a639b4b865b4eb3669e289b7eb/";
  ActualAPIStatus  = new BehaviorSubject<any>("");
  sentences        = new BehaviorSubject<any>("");
  Res1             = new BehaviorSubject<any>(""); 
  Res2             = new BehaviorSubject<any>(""); 
  Res3             = new BehaviorSubject<any>("");
  Res4             = new BehaviorSubject<any>("");
  Res5             = new BehaviorSubject<any>("");
  Res6             = new BehaviorSubject<any>("");
  APIStatus        = {
                      "Sentiment":{"Description":"Sentiment Analysis","value":false},
                      "entities" :{"Description":"Named entities","value":false},
                      "ReGex"    :{"Description":"Regular Expression (ReGex extraction)","value":false},
                      "Semantic" :{"Description":"Semantic Search","value":false}
                     };
  get isActualAPIStatus() {
    return this.ActualAPIStatus.asObservable();
  }
  get isSentences() {
    return this.sentences.asObservable();
  }
  get isRes1() {
    return this.Res1.asObservable();
  }
  get isRes2() {
    return this.Res2.asObservable();
  }
  get isRes3() {
    return this.Res3.asObservable();
  }
  get isRes4() {
    return this.Res4.asObservable();
  }
  get isRes5() {
    return this.Res5.asObservable();
  }
  get isRes6() {
    return this.Res6.asObservable();
  }
  constructor(private http: HttpClient) {}

  getSynonyms(data) : Observable<any> {
    return this.http.get<any>(this.url7 + data + "/json")
  }

  callFirst(data)    : Observable<any> {
    return this.http.post<any>(this.url1,data)
  }
  callSecond(data) : Observable<any> {
    return this.http.get<any>(this.url2 + data)
  }
  callThird(data) : Observable<any> {
    return this.http.post<any>(this.url3,data)
  }
  callFour(data)  : Observable<any> {
    return this.http.post<any>(this.url4,data)
  }
  callFive(data)  : Observable<any> {
    return this.http.post<any>(this.url5,data)
  }
  callSix(data)   : Observable<any> {
    return this.http.post<any>(this.url6,data)
  }
}
