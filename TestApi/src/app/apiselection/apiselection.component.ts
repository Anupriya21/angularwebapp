import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-apiselection',
  templateUrl: './apiselection.component.html',
  styleUrls: ['./apiselection.component.css']
})
export class APISelectionComponent implements OnInit {
  LogForm;
  APIDic;
  sentences;
  file;
  Synonyms;
  wordsen = [];
  APINames = [];
  ActualAPIStatus = {};
  res2;
  IsVisible;
  SearchForm;
  Response2 = {};
  Response5 = {};
  Response4 = [
    {
      "Phones": [],
      "Emails": [
        "firstaidtraining@redcross.org.uk"
      ],
      "PPSs": []
    }
  ];
  Response3 = {
    "derived_date": "2019-12-13 10:41:21.637561",
    "positive": 0.125,
    "neutral": 0.852,
    "negative": 0.023,
    "compound": 0.9963,
    "verdict": "NEUTRAL",
    "verdict_compound": "POSITIVE"
  };
  Response1 = {
    "named_entity_resolution": {
      "People": [
        "Filippo",
        "Filippo Sassi",
        "Patrick Gollop",
        "Bradbury House",
        "Filippo Sassi",
        "Filippo",
        "Filippo Sassi Pass",
        "Da GAQ",
        "Patrick Gollop",
        "Scotiand"
      ],
      "Companies, agencies, institutions": [
        "the British Red Cross",
        "redcross.org.uk/safehands",
        "Dontforget",
        "the Customer Service Team",
        "Mo Aton",
        "the Heai) & Safety",
        "the Intemational Red Cross",
        "Red Crescent Movement",
        "The Bish Red Gross Society",
        "Red Cross Training Regitiofad",
        "Ohio Avenue Salford",
        "M50",
        "Blackthorn Way Ashford",
        "redcross.org.uk/safehands",
        "the British Red Cross",
        "the Customer Service Team",
        "Red Cross Training",
        "the International Red Cross",
        "Red Crescent Movement",
        "The British Red Cross Society",
        "Registered"
      ],
      "Nationalities or religious or political groups": [],
      "Buildings, airports, highways, bridges": [],
      "Countries, cities, states": [
        "England",
        "Wales",
        "Scotland",
        "England",
        "Wales"
      ],
      "Monetary values, including unit": [],
      "Non-GPE locations, mountain ranges, bodies of water": [],
      "Objects, vehicles, foods": [],
      "Named hurricanes, battles, wars, sports events": [
        "Aid Training 00045 4"
      ],
      "Titles of books, songs, arts": [
        "Safe Hands",
        "Version",
        "Safe Hands"
      ],
      "Laws": [],
      "Languages": [],
      "Dates": [
        "30 Aug 2016",
        "Date 29 Aug 2019",
        "1981",
        "arter 1908"
      ],
      "Times": [],
      "Percentages": [
        "0844 412 2739"
      ],
      "Quantities": [],
      "Ordinals": [
        "first",
        "first",
        "first",
        "First",
        "first"
      ],
      "Cardinals": [
        "two",
        "One",
        "up to 12",
        "0844 871",
        "220949",
        "19",
        "one",
        "1908",
        "220949"
      ]
    }
  };
  str: string;
  //   "Barclays misled shareholders and the public about one of the biggest investments in the bank's history, a BBC Panorama investigation has found.The bank announced in 2008 that Manchester City owner Sheikh Mansour had agreed to invest more than £3bn.But the BBC found that the money, which helped Barclays avoid a bailout by British taxpayers, actually came from the Abu Dhabi government.Barclays said the mistake in its accounts was 'a drafting error'.Unlike RBS and Lloyds TSB, Barclays narrowly avoided having to request a government bailout late in 2008 after it was rescued by £7bn worth of new investment, most of which came from the gulf states of Qatar and Abu Dhabi.Half of the cash was supposed to be coming from Sheikh Mansour.But Barclays has admitted it was told the investor might change shortly before shareholders voted to approve the deal on 24 November 2008.But instead of telling shareholders, the bank remained silent until the change of investor was confirmed a few hours later.";
  constructor(private spinner: NgxSpinnerService, private appService: AppService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.IsVisible = false;
    this.SearchForm = new FormGroup({
      Keyword: new FormControl('')
    });
    for (let key in this.Response1["named_entity_resolution"]) {
      if (this.Response1["named_entity_resolution"][key].length) {
        this.Response2[key] = this.Response1["named_entity_resolution"][key];
      }
    }
    for (let key in this.Response4[0]) {
      if (this.Response4[0][key].length) {
        this.Response5[key] = this.Response4[0][key];
      }
    }
    this.APIDic = this.appService.APIStatus;
    for (let key in this.APIDic) {
      this.APINames.push(this.APIDic[key]['Description']);
    }
    console.log(this.APINames);
    let checkboxArray = new FormArray([
      new FormControl(false),
      new FormControl(false),
      new FormControl(false),
      new FormControl(false)]);

    this.LogForm = new FormGroup({
      AnalyzeText: new FormControl(''),
      APICheckBox: checkboxArray
    });
  }

  submit() {
    if (this.LogForm.get('AnalyzeText').value) {
      let senttext = this.LogForm.get('AnalyzeText').value;
      let APICheckedList = this.LogForm.get('APICheckBox').value
      let finalAPIDic = {};
      let index = 0;
      for (let key in this.APIDic) {
        finalAPIDic[key] = this.APIDic[key];
        finalAPIDic[key]['value'] = APICheckedList[index];
        index = index + 1;
      }
      this.spinner.show();
      setTimeout(() => {
        this.IsVisible = true;
        this.sentences = senttext;
        this.ActualAPIStatus = finalAPIDic;
        this.spinner.hide();
      }, 5000);
    }
    else {
      window.alert("Please enter text to analyze");
    }
  }
  makeBold(input, wordsToBold) {
    return input.replace(new RegExp('(\\b)(' + wordsToBold.join('|') + ')(\\b)', 'ig'), '$1<b>$2</b>$3');
  }
  search() {
    console.log(this.LogForm.get('Keyword').value);
    this.appService.getSynonyms(this.LogForm.get('Keyword').value).subscribe(res => {
      console.log(res);
      this.Synonyms = res["adjective"]["sim"];
      console.log(this.Synonyms);
      for (let sen of this.sentences) {
        let tempdic = {};
        tempdic[sen] = [];
        let wordToBold = [];
        let words = sen.split(" ");
        for (let w of words) {
          for (let s of this.Synonyms) {
            if (w.replace(/[^a-zA-Z ]/g, "").match(RegExp(s))) {
              wordToBold.push(w.replace(/[^a-zA-Z ]/g, ""));
              break;
            }
          }
        }
        wordToBold.push(this.LogForm.get('Keyword').value);
        tempdic[sen] = wordToBold;
        this.wordsen.push(tempdic);
      }
      console.log(this.wordsen);
      console.log("-----------------------------");
      for (let i = 0; i < this.sentences.length; i++) {
        console.log(document.getElementById("sent" + i).innerHTML)
        document.getElementById("sent" + i).innerHTML = this.makeBold(Object.keys(this.wordsen[i])[0], this.wordsen[i][Object.keys(this.wordsen[i])[0]]);
      }
      console.log("-----------------------------");
    });
  }

}
