import { Component, OnInit } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { AppService } from '../app.service';
import * as bootstrap from "bootstrap";
import * as $ from 'jquery'
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  file: any;
  Api2 = {};
  SentimentText = "";
  NamedentitiesText = "";
  ReGexText = "";
  SemanticText = "";
  text1;
  text2;
  text3;
  text4;
  text5;
  Text;
  mainNav;
  IsSentiment;
  IsNamedentities
  IsReGex;
  IsSemantic;
  Response2 = {};
  Response5 = {};
  Response4 = [
    {
      "Phones": [],
      "Emails": [
        "firstaidtraining@redcross.org.uk"
      ],
      "PPSs": []
    }
  ];
  Response3 = {
    "derived_date": "2019-12-13 10:41:21.637561",
    "positive": 0.125,
    "neutral": 0.852,
    "negative": 0.023,
    "compound": 0.9963,
    "verdict": "NEUTRAL",
    "verdict_compound": "POSITIVE"
  };
  Response1 = {
    "named_entity_resolution": {
      "People": [
        "Filippo",
        "Filippo Sassi",
        "Patrick Gollop",
        "Bradbury House",
        "Filippo Sassi",
        "Filippo",
        "Filippo Sassi Pass",
        "Da GAQ",
        "Patrick Gollop",
        "Scotiand"
      ],
      "Companies, agencies, institutions": [
        "the British Red Cross",
        "redcross.org.uk/safehands",
        "Dontforget",
        "the Customer Service Team",
        "Mo Aton",
        "the Heai) & Safety",
        "the Intemational Red Cross",
        "Red Crescent Movement",
        "The Bish Red Gross Society",
        "Red Cross Training Regitiofad",
        "Ohio Avenue Salford",
        "M50",
        "Blackthorn Way Ashford",
        "redcross.org.uk/safehands",
        "the British Red Cross",
        "the Customer Service Team",
        "Red Cross Training",
        "the International Red Cross",
        "Red Crescent Movement",
        "The British Red Cross Society",
        "Registered"
      ],
      "Nationalities or religious or political groups": [],
      "Buildings, airports, highways, bridges": [],
      "Countries, cities, states": [
        "England",
        "Wales",
        "Scotland",
        "England",
        "Wales"
      ],
      "Monetary values, including unit": [],
      "Non-GPE locations, mountain ranges, bodies of water": [],
      "Objects, vehicles, foods": [],
      "Named hurricanes, battles, wars, sports events": [
        "Aid Training 00045 4"
      ],
      "Titles of books, songs, arts": [
        "Safe Hands",
        "Version",
        "Safe Hands"
      ],
      "Laws": [],
      "Languages": [],
      "Dates": [
        "30 Aug 2016",
        "Date 29 Aug 2019",
        "1981",
        "arter 1908"
      ],
      "Times": [],
      "Percentages": [
        "0844 412 2739"
      ],
      "Quantities": [],
      "Ordinals": [
        "first",
        "first",
        "first",
        "First",
        "first"
      ],
      "Cardinals": [
        "two",
        "One",
        "up to 12",
        "0844 871",
        "220949",
        "19",
        "one",
        "1908",
        "220949"
      ]
    }
  };

  str: string =
    "For example, instead of using the word “beautiful” several times in your text, you might use synonyms such as “gorgeous,” “stunning,” or “ravishing” to better paint a picture of your description. Using just one word repeatedly will ensure that you will lose the attention of your audience simply out of boredom!It is quite easy to build your arsenal of synonyms, and the list of tools later in this article will help you get started. As with any efforts to increase your vocabulary, it is helpful to keep a journal or list of new words to which to refer. It is also helpful to use those new synonyms often to keep them in your memory. The more you use new words, the more quickly they will come to mind in your oral or written presentations.";
  constructor(private router: Router, private spinner: NgxSpinnerService, private appService: AppService) { }

  toggleCard(id) {
    if ($("#" + id).hasClass('clicked')) {
      $("#" + id).removeClass('clicked');
      $("#" + id).addClass('fa-chevron-down');
      $("#" + id).removeClass('fa-chevron-up');
    }
    else {
      $("#" + id).addClass('clicked');
      $("#" + id).removeClass('fa-chevron-down');
      $("#" + id).addClass('fa-chevron-up');
    }
  }
  ngOnInit() {
    let ref = this;
    $(document).on("click", "i", function () {
      switch (this.id) {
        case "Sentiment":
          if (ref.text1) {
            $("#" + this.id + "-Card").toggleClass("open");
          }
          ref.toggleCard(this.id);
          break;
        case "Namedentities":
          if (ref.text2) {
            $("#" + this.id + "-Card").toggleClass("open");
          }
          ref.toggleCard(this.id);
          break;
        case "ReGex":
          if (ref.text3) {
            $("#" + this.id + "-Card").toggleClass("open");
          }
          ref.toggleCard(this.id);
          break;
        default:
          if (ref.text4) {
            $("#" + this.id + "-Card").toggleClass("open");
          }
          ref.toggleCard(this.id);
          break;
      }
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
  }
  Close(APIIndex, WrapperId, TableId) {
    let TId = $("#" + TableId);
    if ($("#" + TableId).hasClass('open')) {
      TId.removeClass('open');
    }
    let WId = $("#" + WrapperId);
    if ($("#" + WrapperId).hasClass('open')) {
      WId.removeClass('open');
    }
    switch (APIIndex) {
      case 1:
        this.SentimentText = "";
        break;
      case 2:
        this.NamedentitiesText = "";
        break;
      case 3:
        this.ReGexText = "";
        break;
      case 4:
        this.SemanticText = "";
        break;
      default:
        this.Text = "";
    }
  }
  onSubmit(APIIndex, WrapperId, TableId) {
    let TId = $("#" + TableId);
    if ($("#" + TableId).hasClass('open')) {
      TId.removeClass('open');
    }
    let WId = $("#" + WrapperId);
    if ($("#" + WrapperId).hasClass('open')) {
      WId.removeClass('open');
    }
    switch (APIIndex) {
      case 1:
        console.log("It is a Sunday.");
        this.text1 = this.SentimentText;
        if (this.text1) {
          this.spinner.show();
          setTimeout(() => {
            this.spinner.hide();
            WId.addClass('open');
            TId.addClass('open');
          }, 2000);
        }
        else {
          alert("Please enter text to analyze.");
        }
        break;
      case 2:
        console.log("It is a Monday.");
        this.text2 = this.NamedentitiesText;
        if (this.text2) {
          this.spinner.show();
          setTimeout(() => {
            this.spinner.hide();
            WId.addClass('open');
            TId.addClass('open');
            for (let key in this.Response1["named_entity_resolution"]) {
              if (this.Response1["named_entity_resolution"][key].length) {
                this.Response2[key] = this.Response1["named_entity_resolution"][key];
              }
            }
          }, 2000);
        }
        else {
          window.alert("Please enter text to analyze.");
        }

        break;
      case 3:
        console.log("It is a Tuesday.");
        this.text3 = this.ReGexText;
        if (this.text3) {
          this.spinner.show();
          setTimeout(() => {
            this.spinner.hide();
            for (let key in this.Response4[0]) {
              if (this.Response4[0][key].length) {
                this.Response5[key] = this.Response4[0][key];
              }
            }
            // this.appService.callThird(this.text3).subscribe(res => {
            //   this.Response4 = res;
            //   for (let key in this.Response4[0]) {
            //     if (this.Response4[0][key].length) {
            //       this.Response5[key] = this.Response4[0][key];
            //     }
            //   }
            // });
            WId.addClass('open');
            TId.addClass('open');
          }, 2000);
        }
        else {
          window.alert("Please enter text to analyze.");
        }
        break;
      case 4:
        console.log("It is a Wednesday.");
        this.text4 = this.SemanticText;
        if (this.text4) {
          this.spinner.show();
          setTimeout(() => {
            this.spinner.hide();
            WId.addClass('open');
            TId.addClass('open');
          }, 2000);
        }
        else {
          window.alert("Please enter text to analyze.");
        }
        break;
      default:
        this.text5 = this.Text
        if (this.text5) {
          this.spinner.show();
          setTimeout(() => {
            this.spinner.hide();
            WId.addClass('open');
            TId.addClass('open');
          }, 2000);
        }
        else {
          window.alert("Please enter text to analyze.");
        }
    }
  }
}
